## Just a sample login and token-based app using Spring

1. Clone this repo
2. Create MySQL DB `best_login`
3. Create table based on `User` and `Session` entity
4. Insert new user into `user`
5. Run the app

### Sample usage on endpoint
```http request
###
POST http://localhost:8080/dashboard/login
Content-Type:application/json

{
  "username": "myusername",
  "password": "mypassword"
}

###
GET http://localhost:8080/dashboard/welcome
X-Token:9d61aa6c-0739-4950-af4a-1c9997fa959c
```