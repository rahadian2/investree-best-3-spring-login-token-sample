package id.investree.best.bestsamplelogin.repository

import id.investree.best.bestsamplelogin.entity.User
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface UserRepository : JpaRepository<User, Long> {

    fun findByUsernameAndPassword(username: String?, password: String?): User?
}