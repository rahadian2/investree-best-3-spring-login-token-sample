package id.investree.best.bestsamplelogin.repository

import id.investree.best.bestsamplelogin.entity.Session
import id.investree.best.bestsamplelogin.entity.User
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface SessionRepository : JpaRepository<Session, Long> {

    fun findBySessionId(sessionId: String?): Session?

    fun findByUser(user: User?): Session?
}