package id.investree.best.bestsamplelogin.service.auth

import id.investree.best.bestsamplelogin.entity.User

interface AuthenticationService {

    fun login(username: String?, password: String?): String?

    fun getLoggedInUser(token: String?): User?
}