package id.investree.best.bestsamplelogin.service.auth

import id.investree.best.bestsamplelogin.entity.Session
import id.investree.best.bestsamplelogin.entity.User
import id.investree.best.bestsamplelogin.repository.SessionRepository
import id.investree.best.bestsamplelogin.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.time.LocalDateTime
import java.util.UUID

@Service
class AuthenticationServiceImpl @Autowired constructor(
    private val userRepository: UserRepository,
    private val sessionRepository: SessionRepository
) : AuthenticationService {

    override fun login(username: String?, password: String?): String? {
        // the password here is not hashed, very unsecure, consider improving by using SHA256, for example
        val user = userRepository.findByUsernameAndPassword(username, password) ?: return null
        val sessionId = UUID.randomUUID().toString()
        sessionRepository.save(
            Session(
                user = user,
                sessionId = sessionId,
                maxValidTime = LocalDateTime.now().plusMinutes(15L)
            )
        )
        return sessionId
    }

    override fun getLoggedInUser(token: String?): User? {
        return sessionRepository.findBySessionId(token)?.takeIf { LocalDateTime.now().isBefore(it.maxValidTime) }?.user
    }
}