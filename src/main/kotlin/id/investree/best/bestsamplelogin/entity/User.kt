package id.investree.best.bestsamplelogin.entity

import org.hibernate.annotations.DynamicInsert
import org.hibernate.annotations.DynamicUpdate
import javax.persistence.*

@Entity
@Table(name = "user")
@DynamicInsert
@DynamicUpdate
data class User(

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "u_id")
    val id: Long? = null,

    @Column(name = "u_username")
    val username: String? = null,

    @Column(name = "u_passwd")
    val password: String? = null,

    @Column(name = "u_fullname")
    val fullname: String? = null
)
