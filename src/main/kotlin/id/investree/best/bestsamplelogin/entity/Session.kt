package id.investree.best.bestsamplelogin.entity

import org.hibernate.annotations.DynamicInsert
import org.hibernate.annotations.DynamicUpdate
import java.time.LocalDateTime
import javax.persistence.*

@Entity
@Table(name = "session")
@DynamicInsert
@DynamicUpdate
data class Session(

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "s_id")
    val id: Long? = null,

    @ManyToOne
    @JoinColumn(name = "s_u_id")
    val user: User,

    @Column(name = "s_sessid")
    val sessionId: String,

    @Column(name = "s_max_valid_time")
    val maxValidTime: LocalDateTime
)
