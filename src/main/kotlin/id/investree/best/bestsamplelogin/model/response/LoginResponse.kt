package id.investree.best.bestsamplelogin.model.response

import com.fasterxml.jackson.annotation.JsonProperty

data class LoginResponse(

    @JsonProperty("sessionId")
    val sessionId: String?
)
