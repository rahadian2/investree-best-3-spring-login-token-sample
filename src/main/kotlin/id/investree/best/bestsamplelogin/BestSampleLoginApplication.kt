package id.investree.best.bestsamplelogin

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class BestSampleLoginApplication

fun main(args: Array<String>) {
	runApplication<BestSampleLoginApplication>(*args)
}
