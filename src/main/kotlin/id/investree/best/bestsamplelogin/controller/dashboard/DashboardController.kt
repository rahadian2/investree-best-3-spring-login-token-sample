package id.investree.best.bestsamplelogin.controller.dashboard

import id.investree.best.bestsamplelogin.model.request.LoginRequest
import id.investree.best.bestsamplelogin.model.response.LoginResponse
import id.investree.best.bestsamplelogin.service.auth.AuthenticationService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestHeader
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping(value = ["/dashboard"], produces = [MediaType.APPLICATION_JSON_VALUE])
class DashboardController @Autowired constructor(
    private val authenticationService: AuthenticationService
) {

    @PostMapping(value = ["login"])
    fun login(
        @RequestBody request: LoginRequest
    ): ResponseEntity<Any> {
        val sessionId = authenticationService.login(request.username, request.password)
            ?: return ResponseEntity("Invalid username/password", HttpStatus.UNAUTHORIZED)
        return ResponseEntity(LoginResponse(sessionId), HttpStatus.OK)
    }

    @GetMapping(value = ["welcome"])
    fun welcome(
        @RequestHeader("X-Token") token: String?
    ): ResponseEntity<Any> {
        val user = authenticationService.getLoggedInUser(token)
            ?: return ResponseEntity(HttpStatus.UNAUTHORIZED)
        return ResponseEntity("Welcome aboard, ${user.fullname}!", HttpStatus.OK)
    }
}